package Task07_String_RegEx;

import java.util.Arrays;

public class Sentences {
	private String words[];

	public Sentences(String[] words) {
		
		this.words = words;
	}

	public String[] getWords() {
		return words;
	}

	public void setWords(String[] words) {
		this.words = words;
	}

	@Override
	public String toString() {
		return "Sentences [words=" + Arrays.toString(words) + "]";
	}
	
	
	
}
