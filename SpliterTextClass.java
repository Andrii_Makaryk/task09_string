package Task07_String_RegEx;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class SpliterTextClass {
	public static String readFile() {
		String path = "E://book.txt";
		String line;
		File filePath = new File(path);
		StringBuilder text = new StringBuilder();
		try (BufferedReader bf = new BufferedReader(new InputStreamReader(new FileInputStream(filePath)))) {
			while ((line = bf.readLine()) != null) {
				text.append(line + '\n');
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return text.toString();
	}

	public List<Sentences> parseTextToCollaction() {

		List<Sentences> sentntences = new ArrayList<>();
		StringTokenizer tokenizer1 = new StringTokenizer(readFile(), ".");
		while (tokenizer1.hasMoreTokens()) {
			String[] words = new String[tokenizer1.countTokens()];
			for (int i = 0; i < words.length; i++) {
				words[i] = tokenizer1.nextToken();
				sentntences.add(new Sentences(words));
			}
		}
		return sentntences;
	}

	// 1. Знайти найбільшу кількість речень тексту, в яких є однакові слова.
	public int foundMaxSentens(List<Sentences> list) {
		int counter = 0;
		for (Sentences st : list) {
			for (int i = 0; i < st.getWords().length; i++) {
				if (st.getWords()[i].equals(st.getWords()[i]))
					counter++;
			}
		}
		return counter;
	}

	// 2. Вивести всі речення заданого тексту у порядку зростання кількості слів у
	// кожному з них.
	public void sequanceOfWord(List<Sentences> list) {
		list.stream().sorted((a, b) -> a.getWords().length - b.getWords().length).forEach(p -> {
			System.out.println(p.toString());
		});
	}
	// 3. Знайти таке слово у першому реченні, якого немає ні в одному з інших
	// речень.

	public void findUniqueWord(List<Sentences> list) {
		List<String> distList = new ArrayList<>();
		String[] firstSentence = list.get(0).getWords();
		for (String word : firstSentence) {
			boolean isUnique = true;
			for (int i = 1; i < list.size(); i++) {
				String[] words = list.get(i).getWords();
				if (Arrays.stream(words).anyMatch(p -> (p.equals(word)))) {
					isUnique = false;
					continue;
				}
			}
			if (isUnique)
				distList.add(word);
		}
		for (String tmp : distList) {
			System.out.println(tmp);
		}
	}

	// 4. У всіх запитальних реченнях тексту знайти і надрукувати без повторів слова
	// заданої довжини.
	public void printWordsOfFixedLength(List<Sentences> list, int size) {
		Set<String> words = new HashSet<>();
		for (Sentences st : list) {
			for (int i = 0; i < st.getWords().length; i++) {
				if (st.getWords()[i].length() == size)
					words.add(st.getWords()[i]);
			}
		}
		for(String str:words) {
			System.out.println(str+" ");
		}
	}

	// 5. У кожному реченні тексту поміняти місцями перше слово, що починається на
	// голосну букву з найдовшим словом.
	public void replace(List<Sentences> list) {		
		String someWord = "";
		int max = 0;
		String longestWord = "";
		for (Sentences st : list) {
			for (int i = 0; i < st.getWords().length; i++) {
				if (st.getWords()[i].startsWith("\\b[AaEeIiOoUuYy][\\wA-Za-z\\-_']*\\b")) {
					someWord = st.getWords()[i];				
				} else if (st.getWords()[i].length() > max) {
					max = st.getWords()[i].length();
					longestWord = st.getWords()[i];
				}
				st.getWords()[i].replaceFirst(someWord, longestWord);
			}
		}
	}
	
}
